<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Master_barang;
use App\Models\Transaksi_pembelian;
use App\Http\Requests\StoreTransaksi_pembelianRequest;
use App\Http\Requests\UpdateTransaksi_pembelianRequest;

class TransaksiPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transaksi.transaksi_pembelian', [
            'barangs' => Master_barang::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransaksi_pembelianRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransaksi_pembelianRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi_pembelian $transaksi_pembelian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi_pembelian $transaksi_pembelian)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransaksi_pembelianRequest  $request
     * @param  \App\Models\Transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransaksi_pembelianRequest $request, Transaksi_pembelian $transaksi_pembelian)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi_pembelian  $transaksi_pembelian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi_pembelian $transaksi_pembelian)
    {
        //
    }

}
