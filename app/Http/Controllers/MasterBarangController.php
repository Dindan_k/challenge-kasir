<?php

namespace App\Http\Controllers;

use App\Models\Master_barang;
use App\Http\Requests\StoreMaster_barangRequest;
use App\Http\Requests\UpdateMaster_barangRequest;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('barang.barang', [
            'barang' => Master_barang::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMaster_barangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMaster_barangRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function show(Master_barang $master_barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Master_barang $master_barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMaster_barangRequest  $request
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMaster_barangRequest $request, Master_barang $master_barang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Master_barang  $master_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Master_barang $master_barang)
    {
        //
    }
}
