<?php

namespace App\Http\Controllers;

use App\Models\Transaksi_pembelian_barang;
use App\Http\Requests\StoreTransaksi_pembelian_barangRequest;
use App\Http\Requests\UpdateTransaksi_pembelian_barangRequest;

class TransaksiPembelianBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransaksi_pembelian_barangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransaksi_pembelian_barangRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransaksi_pembelian_barangRequest  $request
     * @param  \App\Models\Transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransaksi_pembelian_barangRequest $request, Transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi_pembelian_barang  $transaksi_pembelian_barang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi_pembelian_barang $transaksi_pembelian_barang)
    {
        //
    }
}
