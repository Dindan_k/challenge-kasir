<?php

namespace Database\Seeders;
use App\Models\Master_barang;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Master_barang::create([
            'nama_barang' => 'Sabun Batang',
            'harga_satuan' => '3000'
        ]);
        Master_barang::create([
            'nama_barang' => 'Mie Instan',
            'harga_satuan' => '2000'
        ]);
        Master_barang::create([
            'nama_barang' => 'Pensil',
            'harga_satuan' => '1000'
        ]);
        Master_barang::create([
            'nama_barang' => 'Kopi',
            'harga_satuan' => '1500'
        ]);
        Master_barang::create([
            'nama_barang' => 'Air Minum Galon',
            'harga_satuan' => '20000'
        ]);
    }
}
