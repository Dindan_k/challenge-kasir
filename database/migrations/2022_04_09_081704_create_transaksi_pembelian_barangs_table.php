<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiPembelianBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_pembelian_barangs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('transaksi_pembelian_id');
            $table->unsignedBigInteger('master_barang_id');
            $table->integer('jumlah');
            $table->integer('harga_satuan');
            $table->foreign('transaksi_pembelian_id')->references('id')->on('transaksi_pembelians')->onDelete('cascade');
            $table->foreign('master_barang_id')->references('id')->on('master_barangs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_pembelian_barangs');
    }
}
