@extends('master.main')
@section('container')
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <div class="row">
        <div class="col-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Form Barang</h6>
                </div>
                <div class="card-body">
                    <form name="datapribadi" method="" action="">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Nama Barang</label>
                            <select class="form-control" name="nama_barang" id="nama_barang">
                            @foreach ($barangs as $item)
                            <option>{{ $item->nama_barang }}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Harga Satuan</label>
                            <input type="text" class="form-control" name="harga_satuan" id="harga" value="{{ $item->harga_satuan }}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Qty</label>
                            <input type="number" class="form-control" name="qty" id="exampleFormControlInput1" value="1">
                        </div>
                        <div class="form-group">
                            <button type="button" onClick='inputdata()' class="btn btn-success">Simpan</button>
                            <button type="reset" class="btn btn-warning">Batal</button>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Table Transaksi</h6>
                </div>
                <div class="card-body">
                    <table class="table" id="databel">
                        <thead>
                            <tr>
                            <th scope="col">Nama Barang</th>
                            <th scope="col">Harga Satuan</th>
                            <th scope="col">Qty</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </form>
            </div>
            <div class="row">
                <!-- accepted payments column -->
                <div class="col-4">
                </div>
                <!-- /.col -->
                <div class="col-8">
                  <p class="lead">Amount Due 2/22/2014</p>

                  <div class="table-responsive">
                    <table class="table" id="databel2">
                      <tr>
                        <th style="width:50%">Subtotal:</th>
                      </tr>
                      <tr>
                        <th>Disc:</th>
                      </tr>
                      <tr>
                        <th>Total:</th>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
            </div>
        </div>
    </div>
</div>
@endsection
