<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SB Admin 2 - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="/layout/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="/layout/css/sb-admin-2.min.css" rel="stylesheet">
    <!-- Custom styles for this page -->
    <link href="/layout/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    @stack('css')
    <script >
      function inputdata(){
      var nama_barang=document.forms['datapribadi']['nama_barang'].value;
      var harga_satuan=document.forms['datapribadi']['harga_satuan'].value;
      var qty=document.forms['datapribadi']['qty'].value;
      var tabel = document.getElementById("databel");
      var baris = tabel.insertRow(1);
      var kol1 = baris.insertCell(0);
      var kol2 = baris.insertCell(1);
      var kol3 = baris.insertCell(2);

      kol1.innerHTML = nama_barang;
      kol2.innerHTML = harga_satuan;
      kol3.innerHTML = qty;
     }
    </script>

</head>

<body id="page-top">
    <div id="wrapper">
    @include('layout.side')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
        @include('layout.nav')
        @yield('container')
        </div>
    @include('layout.footer')
    </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
    </a>
    @include('modal.logout')
    <!-- Bootstrap core JavaScript-->
    <script src="/layout/vendor/jquery/jquery.min.js"></script>
    <script src="/layout/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="/layout/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="/layout/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="/layout/vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="/layout/js/demo/chart-area-demo.js"></script>
    <script src="/layout/js/demo/chart-pie-demo.js"></script>
    <!-- Page level plugins -->
    <script src="/layout/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="/layout/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="/layout/js/demo/datatables-demo.js"></script>
    @stack('scripts')

</body>

</html>
